package models

type Publication struct {
	ID          int64  `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Images      string `json:"images"`
	Contact     string `json:"contact"`
	Tag         string `json:"tag"`
}
