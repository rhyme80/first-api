package services

import (
	dbConnection "../../db"
	"../models"
)

func CreatePublication(publication models.Publication) error {
	db, err := dbConnection.GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec("INSERT INTO publications (title, description, images, contact, tag) VALUES (?, ?, ?, ?, ?)", publication.Title, publication.Description, publication.Images, publication.Contact, publication.Tag)
	if err != nil {
		return err
	}
	return err
}

func GetPublicationByID(id int64) (*models.Publication, error) {
	var publication models.Publication
	db, err := dbConnection.GetDB()
	if err != nil {
		return nil, err
	}
	row := db.QueryRow("SELECT id, title, description, images, contact, tag FROM publications WHERE id  = ?", id)
	err = row.Scan(&publication.ID, &publication.Title, &publication.Description, &publication.Images, &publication.Contact, &publication.Tag)
	if err != nil {
		return nil, err
	}
	return &publication, nil
}

func GetPublications() ([]models.Publication, error) {
	var publications []models.Publication
	//publications := []models.Publication
	db, err := dbConnection.GetDB()
	if err != nil {
		return nil, err
	}
	rows, err := db.Query("SELECT id, title, description, images, contact, tag FROM publications")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var publication models.Publication
		err = rows.Scan(&publication.ID, &publication.Title, &publication.Description, &publication.Images, &publication.Contact, &publication.Tag)
		if err != nil {
			return nil, err
		}
		publications = append(publications, publication)
	}
	return publications, nil
}

func UpdatePublication(publication models.Publication) error {
	db, err := dbConnection.GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec("UPDATE publications SET title = ?, description = ?, images = ?, contact = ?, tag = ? WHERE id = ?", publication.Title, publication.Description, publication.Images, publication.Contact, publication.Tag, publication.ID)
	if err != nil {
		return err
	}
	return nil
}

func DeletePublication(id int64) error {
	db, err := dbConnection.GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec("DELETE FROM publications WHERE id = ?", id)
	return nil
}
