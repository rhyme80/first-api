package handlers

import (
	configCommon "../../config"
	"fmt"
	"github.com/google/uuid"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func UploadFile(w http.ResponseWriter, r *http.Request) {
	_ = r.ParseMultipartForm(1000)
	file, fileInfo, err := r.FormFile("file")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(file)
	fmt.Println("---")
	fmt.Println(fileInfo)

	defer file.Close()

	uuid := uuid.New().String()
	nameFileTmp := strings.Replace(uuid, "-", "", -1)
	extensionFile := filepath.Ext(fileInfo.Filename)
	nameFile := "./tmp/files/" + nameFileTmp + extensionFile

	f, err := os.OpenFile(nameFile, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	io.Copy(f, file)

	if err == nil {
		configCommon.RespondWithSuccess(nameFile, w)
	} else {
		configCommon.RespondWithError(err, w)
	}
}
