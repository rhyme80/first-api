package handlers

import (
	configCommon "../../config"
	"../../utils"
	"../models"
	"../services"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

func GetPublications(w http.ResponseWriter, r *http.Request) {
	publications, err := services.GetPublications()
	if err == nil {
		configCommon.RespondWithSuccess(publications, w)
	} else {
		configCommon.RespondWithError(err, w)
	}
}

func GetPublication(w http.ResponseWriter, r *http.Request) {
	idAsString := mux.Vars(r)["id"]
	id, err := utils.StringToInt64(idAsString)
	if err != nil {
		configCommon.RespondWithError(err, w)
		return
	}
	publication, err := services.GetPublicationByID(id)
	if err != nil {
		configCommon.RespondWithError(err, w)
	} else {
		configCommon.RespondWithSuccess(publication, w)
	}
}

func PostPublication(w http.ResponseWriter, r *http.Request) {
	var publication models.Publication
	err := json.NewDecoder(r.Body).Decode(&publication)
	if err != nil {
		configCommon.RespondWithError(err, w)
	} else {
		err := services.CreatePublication(publication)
		if err != nil {
			configCommon.RespondWithError(err, w)
		} else {
			configCommon.RespondWithSuccess(true, w)
		}
	}
}

func PutPublication(w http.ResponseWriter, r *http.Request) {
	var publication models.Publication
	err := json.NewDecoder(r.Body).Decode(&publication)
	if err != nil {
		configCommon.RespondWithError(err, w)
	} else {
		err := services.UpdatePublication(publication)
		if err != nil {
			configCommon.RespondWithError(err, w)
		} else {
			configCommon.RespondWithSuccess(true, w)
		}
	}
}

func DeletePublication(w http.ResponseWriter, r *http.Request) {
	idAsString := mux.Vars(r)["id"]
	id, err := utils.StringToInt64(idAsString)
	if err != nil {
		configCommon.RespondWithError(err, w)
		return
	}
	err = services.DeletePublication(id)
	if err != nil {
		configCommon.RespondWithError(err, w)
	} else {
		configCommon.RespondWithSuccess(true, w)
	}
}