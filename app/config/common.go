package handlers

import (
	"encoding/json"
	"net/http"
)

// Helper functions for respond with 200 or 500 code
func RespondWithError(err error, writer http.ResponseWriter) {
	writer.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(writer).Encode(err.Error())
}

func RespondWithSuccess(data interface{}, writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	json.NewEncoder(writer).Encode(data)
}
