package handlers

import (
	config "../../config"
	"../../utils"
	"../models"
	"../services"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

func GetUsers(w http.ResponseWriter, r *http.Request) {
	user, err := services.GetUsers()
	if err == nil {
		config.RespondWithSuccess(user, w)
	} else {
		config.RespondWithError(err, w)
	}
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	idAsString := mux.Vars(r)["id"]
	id, err := utils.StringToInt64(idAsString)
	if err != nil {
		config.RespondWithError(err, w)
		return
	}
	user, err := services.GetUserByID(id)
	if err == nil {
		config.RespondWithSuccess(user, w)
	} else {
		config.RespondWithError(err, w)
	}
}

func PostUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		config.RespondWithError(err, w)
	} else {
		err := services.CreateUser(user)
		if err != nil {
			config.RespondWithError(err, w)
		} else {
			config.RespondWithSuccess(true, w)
		}
	}
}

func PutUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		config.RespondWithError(err, w)
	} else {
		err := services.UpdateUser(user)
		if err != nil {
			config.RespondWithError(err, w)
		} else {
			config.RespondWithSuccess(true, w)
		}
	}
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	idAsString := mux.Vars(r)["id"]
	id, err := utils.StringToInt64(idAsString)
	if err != nil {
		config.RespondWithError(err, w)
		return
	}
	err = services.DeleteUser(id)
	if err != nil {
		config.RespondWithError(err, w)
	} else {
		config.RespondWithSuccess(true, w)
	}
}

func Login(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		config.RespondWithError(err, w)
	} else {
		token, err := services.LoginUser(user)
		if err != nil {
			config.RespondWithError(err, w)
		} else {
			config.RespondWithSuccess(token, w)
		}
	}
}

//func Signup(w http.ResponseWriter, r *http.Request) {
//	var user models.User
//	err := json.NewDecoder(r.Body).Decode(&user)
//	if err != nil {
//		config.RespondWithError(x|err, w)
//	} else {
//		err := services.LoginUser(user)
//		if err != nil {
//			config.RespondWithError(err, w)
//			http.Redirect(w, r, "/login", 301)
//		} else {
//			config.RespondWithSuccess(true, w)
//		}
//	}
//}
