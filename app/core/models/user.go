package models

import "github.com/dgrijalva/jwt-go"

type User struct {
	ID       int64  `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

type Claim struct {
	User `json:"user"`
	jwt.StandardClaims
}

type ResponseToken struct {
	Token string `json:"token"`
}