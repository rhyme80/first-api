package services

import (
	dbConnection "../../db"
	"../models"
	"golang.org/x/crypto/bcrypt"
)

func CreateUser(user models.User) error {
	db, err := dbConnection.GetDB()
	if err != nil {
		return err
	}
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	_, err = db.Exec("INSERT INTO users (id, username, password) VALUES (?, ?, ?)", user.ID, user.Username, hashedPassword)
	if err != nil {
		return err
	}
	return err
}

func GetUserByID(id int64) (*models.User, error) {
	var user models.User
	db, err := dbConnection.GetDB()
	if err != nil {
		return nil, err
	}
	row := db.QueryRow("SELECT id, username, password FROM users WHERE id = ?", user.ID)
	err = row.Scan(&user.ID, &user.Username, &user.Password)
	if err != nil {
		return nil, err
	}
	return &user, nil
}

func GetUsers() ([]models.User, error) {
	var users []models.User
	db, err := dbConnection.GetDB()
	if err != nil {
		return nil, err
	}
	rows, err := db.Query("SELECT id, username, password FROM users")
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var user models.User
		err = rows.Scan(&user.ID, &user.Username, &user.Password)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}

func UpdateUser(user models.User) error {
	db, err := dbConnection.GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec("UPDATE users SET username = ?, password = ? WHERE id=?", user.Username, user.Password, user.ID)
	if err != nil {
		return err
	}
	return nil
}

func DeleteUser(id int64) error {
	db, err := dbConnection.GetDB()
	if err != nil {
		return err
	}
	_, err = db.Exec("DELETE FROM users WHERE id = ?", id)
	if err != nil {
		return err
	}
	return nil
}
