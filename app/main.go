package main

import (
	"./db"
	"./routers"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

func main() {
	// ping database
	db_, err := db.GetDB()
	if err != nil {
		log.Printf("error with database" + err.Error())
	} else {
		err = db_.Ping()
		if err != nil {
			fmt.Println(&db_)
			log.Printf("Error making connection to DB. Please check credentials. The error is: " + err.Error())
			return
		}
	}

	// Define routers
	router := mux.NewRouter()
	routers.SetRouters(router)

	// Setup and start server
	port := ":8080"

	server := &http.Server{
		Handler: router,
		Addr:    port,
		// timeouts so the server never waits forever...
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Printf("Server started at %s", port)
	log.Fatal(server.ListenAndServe())
}
