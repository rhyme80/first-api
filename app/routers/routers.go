package routers

import (
	handlersCore "../core/handlers"
	handlersTopAds "../top_ads/handlers"
	"github.com/gorilla/mux"
	"net/http"
)

func SetRouters(router *mux.Router) {
	// Enable CORS
	enableCORS(router)

	// Publications
	Get(router, "/publications", handleRequest(handlersTopAds.GetPublications))
	Get(router, "/publications/{id}", handleRequest(handlersTopAds.GetPublication))
	Post(router, "/publications", handleRequest(handlersTopAds.PostPublication))
	Put(router, "/publications", handleRequest(handlersTopAds.PutPublication))
	Delete(router, "/publications/{id}", handleRequest(handlersTopAds.DeletePublication))

	// Users
	Get(router, "/users", handleRequest(handlersCore.GetUsers))
	Get(router, "/users/{id}", handleRequest(handlersCore.GetUser))
	Post(router, "/users", handleRequest(handlersCore.PostUser))
	Put(router, "/users", handleRequest(handlersCore.PutUser))
	Delete(router, "/users/{id}", handleRequest(handlersCore.DeleteUser))
	Post(router, "/login", handleRequest(handlersCore.Login))

	// File
	Post(router, "/files", handleRequest(handlersTopAds.UploadFile))

}

type RequestHandlerFunction func(w http.ResponseWriter, r *http.Request)

func handleRequest(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		handler(w, r)
	}
}

func enableCORS(router *mux.Router) {
	router.PathPrefix("/").HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		writer.Header().Set("Access-Control-Allow-Origin", "*")
	}).Methods(http.MethodOptions)
	router.Use(middlewareCors)
}

func middlewareCors(next http.Handler) http.Handler {
	return http.HandlerFunc(
		func(writer http.ResponseWriter, request *http.Request) {
			// Just put some headers to allow CORS...
			writer.Header().Set("Access-Control-Allow-Origin", "*")
			writer.Header().Set("Access-Control-Allow-Credentials", "true")
			writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			writer.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
			// and call next handler
			next.ServeHTTP(writer, request)
		},
	)
}

// Methods
// Get wraps the router for GET method
func Get(router *mux.Router, path string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(path, f).Methods("GET")
}

// Post wraps the router for POST method
func Post(router *mux.Router, path string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(path, f).Methods("POST")
}

// Put wraps the router for PUT method
func Put(router *mux.Router, path string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(path, f).Methods("PUT")
}

// Delete wraps the router for DELETE method
func Delete(router *mux.Router, path string, f func(w http.ResponseWriter, r *http.Request)) {
	router.HandleFunc(path, f).Methods("DELETE")
}
